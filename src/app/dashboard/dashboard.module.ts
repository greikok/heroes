import { SharedModule } from '../shared/modules/shared.module';
import { DashboardRoutes } from './dashboard.routing';
import { DashboardComponent } from './dashboard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    DashboardComponent,
    NotfoundComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutes,
    SharedModule
  ]
})
export class DashboardModule { }
