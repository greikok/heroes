import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundComponent } from './components/notfound/notfound.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {
      breadcrumb: {
        label: 'app home',
        info: 'home'
      }
    },
    children: [
      {
        path: '',
        redirectTo: 'heroes',
        pathMatch: 'full'
      },
      {
        path: 'notFound',
        component: NotfoundComponent
      },
      {
        path: 'heroes',
        loadChildren: () => import('../modules/heroes/heroes.module').then(m => m.HeroesModule)
      }
    ]
  },
];

export const DashboardRoutes = RouterModule.forChild(routes);
