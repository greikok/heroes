import { CONSTANTS } from './../../../shared/utils/const';
import { Component } from '@angular/core';

@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.scss']
})
export class NotfoundComponent {
  CONST = CONSTANTS
}
