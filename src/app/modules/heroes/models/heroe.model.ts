export interface Heroe {
  id: number,
  name: string,
  nickname?: string,
  color?: string,
  weight?: string,
  height?: string,
  country?: string
}
