import { AlertType } from './../../../shared/utils/enum';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Heroe } from '../models/heroe.model';
import { AlertService } from 'src/app/shared/services/alert.service';

@Injectable({
  providedIn: 'root'
})
export class HeroeService {
  private heroesUrl = 'api/heroes';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient, private alertService: AlertService) { }

  /** GET heroes from the server */
  getHeroes(): Observable<Heroe[]> {
    return this.http.get<Heroe[]>(this.heroesUrl)
      .pipe(
        catchError(this.handleError<Heroe[]>('getHeroes', []))
      );
  }

  /** GET hero by id. Return `undefined` when id not found */
  getHeroNo404<Data>(id: number): Observable<Heroe> {
    const url = `${this.heroesUrl}/?id=${id}`;
    return this.http.get<Heroe[]>(url)
      .pipe(
        map(heroes => heroes[0]), // returns a {0|1} element array
        catchError(this.handleError<Heroe>(`getHero id=${id}`))
      );
  }

  /** GET hero by id. Will 404 if id not found */
  getHero(id: number): Observable<Heroe> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Heroe>(url).pipe(
      catchError(this.handleError<Heroe>(`getHero id=${id}`))
    );
  }

  //////// Save methods //////////

  /** POST: add a new hero to the server */
  addHero(hero: Heroe): Observable<Heroe> {
    return this.http.post<Heroe>(this.heroesUrl, hero, this.httpOptions).pipe(
      catchError(this.handleError<Heroe>('addHero'))
    );
  }

  /** DELETE: delete the hero from the server */
  deleteHero(id: number): Observable<Heroe> {
    const url = `${this.heroesUrl}/${id}`;

    return this.http.delete<Heroe>(url, this.httpOptions).pipe(
      catchError(this.handleError<Heroe>('deleteHero'))
    );
  }

  /** PUT: update the hero on the server */
  updateHero(hero: Heroe): Observable<any> {
    return this.http.put(this.heroesUrl, hero, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateHero'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.alertService.alert(`Error in ${operation}`, AlertType.error)
      return of(result as T);
    };
  }

}
