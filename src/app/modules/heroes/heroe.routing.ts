import { HeroeDetailsComponent } from './components/heroe-details/heroe-details.component';
import { HeroesComponent } from './heroes.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: HeroesComponent,
    data: {
      breadcrumb: {
        info: 'home'
      }
    }
  },
  {
    path: 'details/:id', component: HeroeDetailsComponent,
    data: {
      breadcrumb: {
        alias: 'heroes'
      }
    }
  },
  {
    path: 'create', component: HeroeDetailsComponent
  }
];

export const HeroeRoutes = RouterModule.forChild(routes);
