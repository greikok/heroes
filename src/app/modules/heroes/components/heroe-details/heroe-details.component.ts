import { AlertService } from 'src/app/shared/services/alert.service';
import { HeroeService } from './../../services/heroe.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Heroe } from './../../models/heroe.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreadcrumbService } from 'xng-breadcrumb';
import { AlertType } from 'src/app/shared/utils/enum';
import { CONSTANTS } from 'src/app/shared/utils/const';

@Component({
  selector: 'app-heroe-details',
  templateUrl: './heroe-details.component.html',
  styleUrls: ['./heroe-details.component.scss']
})
export class HeroeDetailsComponent implements OnInit {

  heroeForm: FormGroup | undefined;
  editHeroe: Boolean = false;
  CONST = CONSTANTS;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private heroeService: HeroeService,
    private breadcrumbService: BreadcrumbService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.initFormHeroe();
    this.route.snapshot.params['id'] && this.setHeroeValues(this.route.snapshot.params['id']);
  }

  initFormHeroe() {
    this.heroeForm = this.formBuilder.group({
      name: ['', Validators.required],
      color: [''],
      country: [''],
      height: [''],
      weight: [''],
      nickname: ['']
    })
  }

  setHeroeValues(id: number) {
    this.heroeService.getHero(id).subscribe((heroe) => {
      if (heroe) {
        this.heroeForm?.patchValue(heroe);
        this.editHeroe = true;
        this.breadcrumbService.set('@heroes', heroe.name)
      } else {
        this.backHome();
      }
    })
  }

  updateHeroe() {
    const heroe: Heroe = Object.assign({ id: Number(this.route.snapshot.params['id']) }, this.heroeForm?.value);
    this.heroeService.updateHero(heroe).subscribe((test) => {
      this.alertService.alert(this.CONST.UPDATE_SUCCESS, AlertType.success);
      this.backHome();
    });
  }

  createHeroe() {
    this.heroeService.addHero(this.heroeForm?.value).subscribe((test) => {
      this.backHome();
    });
  }

  backHome() {
    this.router.navigate(['heroes/']);
  }

}
