import { HeroeDetailsComponent } from './components/heroe-details/heroe-details.component';
import { HeroeRoutes } from './heroe.routing';
import { SharedModule } from './../../shared/modules/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroesComponent } from './heroes.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HeroeRoutes,
    FormsModule, ReactiveFormsModule,
  ],
  declarations: [HeroesComponent, HeroeDetailsComponent]
})
export class HeroesModule { }
