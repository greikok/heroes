import { AlertType } from './../../shared/utils/enum';
import { AlertService } from 'src/app/shared/services/alert.service';
import { LoaderService } from './../../shared/services/loader.service';
import { HeroeService } from './services/heroe.service';
import { CONSTANTS } from './../../shared/utils/const';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Heroe } from './models/heroe.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = [];
  dataSource: MatTableDataSource<Heroe> = new MatTableDataSource();
  private heroesSuscriptions: Subscription[] = [];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  heroes: Heroe[] = [];
  CONST = CONSTANTS;

  constructor(
    private heroeService: HeroeService,
    private loaderService: LoaderService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private alertService: AlertService
  ) {
    this.dataSource = new MatTableDataSource(this.heroes);
  }
  ngOnInit(): void {
    this.getHeroes();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.heroesSuscriptions.forEach((sub) => {
      sub.unsubscribe()
    })
  }

  getHeroes() {
    this.heroesSuscriptions.push(
      this.heroeService.getHeroes().subscribe({
        next: (result) => {
          this.heroes = result;
          this.displayedColumns = this.getDisplayedColumns(this.heroes);
          this.dataSource.data = result;
          this.loaderService.hiddeLoader();
        }
      })
    )
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getDisplayedColumns(data: Heroe[]): string[] {
    const displayedColumns = Object.keys(data[0]);
    displayedColumns.push(this.CONST.ACTIONS)
    return displayedColumns
  }

  createHeroe() {
    this.router.navigate([`create/`], { relativeTo: this.route });
  }

  editHeroe(id: number) {
    this.router.navigate([`details/${id}`], { relativeTo: this.route });
  }

  deleteHeroe(heroe: Heroe) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: this.CONST.DELETE_HEROE });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.heroeService.deleteHero(heroe.id).subscribe(_ => {
          if (_ === null) {
            this.heroes = this.heroes.filter(h => h !== heroe);
            this.dataSource.data = this.heroes;
            this.alertService.alert(this.CONST.DELETE_SUCCESS, AlertType.success);
          }
        });
      }
    })
  }
}
