export class CONSTANTS {
  public static PAGE_NOT_FOUND = 'No hemos podido encontrar la página';
  public static ACTIONS = 'Acciones';
  public static EMPTY_TABLE = 'No hay datos que coincidan con el filtro';
  public static HEROE_TITLE = 'Lista de Heroes';
  public static DELETE_HEROE = 'Estas seguro que deseas eliminar este super heroe';
  public static DELETE_SUCCESS = 'El heroe se elimino correctamente';
  public static UPDATE_SUCCESS = 'El heroe se modifico correctamente';
  // BUTTONS
  public static CREATE = 'Crear';
  public static EDIT = 'Editar';
  public static DELETE = 'Borrar';
  public static HEROE_ADD = 'Agregar';
}
