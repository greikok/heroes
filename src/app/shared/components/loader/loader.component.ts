import { Observable } from 'rxjs';
import { LoaderService } from './../../services/loader.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
  @Input() isShowLoader$: Observable<boolean> = this.loaderService.showLoading;
  constructor(private loaderService: LoaderService) { }
}
