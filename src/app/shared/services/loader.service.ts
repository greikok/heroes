import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public showLoading = new BehaviorSubject(true);

  showLoader() {
    this.showLoading.next(false);
  }

  hiddeLoader() {
    this.showLoading.next(true);
  }

}
