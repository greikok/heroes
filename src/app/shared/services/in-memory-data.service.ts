import { Heroe } from './../../modules/heroes/models/heroe.model';
import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes: Heroe[] = [
      { id: 12, name: 'Dr. Nice', color: 'red', country: 'Portugal', height: '120cm', weight: '20kg', nickname: 'DR' },
      { id: 13, name: 'Bombasto', color: 'blue', country: 'Brasil', height: '12cm', weight: '11kg', nickname: 'BO' },
      { id: 14, name: 'Celeritas', color: 'y', country: 'Spain', height: '12cm', weight: '421kg', nickname: 'CE' },
      { id: 15, name: 'Magneta', color: 'blue', country: 'Portugal', height: '512cm', weight: '421kg', nickname: 'MA' },
      { id: 16, name: 'RubberMan', color: 'red', country: 'Spain', height: '112cm', weight: '217kg', nickname: 'RU' },
      { id: 17, name: 'Dynama', color: 'red', country: 'Spain', height: '122cm', weight: '241kg', nickname: 'DY' },
      { id: 18, name: 'Dr. IQ', color: 'red', country: 'Portugal', height: '12cm', weight: '721kg', nickname: 'DR Q' },
      { id: 19, name: 'Magma', color: 'black', country: 'Spain', height: '11cm', weight: '214kg', nickname: 'MAG' },
      { id: 20, name: 'Tornado', color: 'red', country: 'Portugal', height: '12cm', weight: '214kg', nickname: 'TOR' }
    ];
    return { heroes };
  }

  genId(heroes: Heroe[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}
